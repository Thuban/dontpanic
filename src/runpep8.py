#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import pep8


class CodeCheck(unittest.TestCase):

    def test_pep8(self):
        """Proceed a pep8 checking"""
        pep8style = pep8.StyleGuide(config_file='.pep8')

        results = pep8style.check_files(['.'])
        self.assertEqual(results.get_statistics(), [], "Found code  errors (and warnings).")

if __name__ == '__main__':
    unittest.main()
