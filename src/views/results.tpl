<!doctype html>
<html>
%include('head.tpl')
<body>
    <header>
        <h1>{{tpl_schres}}</h1>
        <nav>
            %include('backtomain.tpl')
        </nav>
    </header>

    <main>
        <form class="searchbar" action="/search" method="get">
            <input name="keywords" type="text"  value="{{the_search}}"/>
            <input name="isserie" type="hidden"  value="{{isserie}}"/>
            {{!search_engines}}
            <input value="{{tpl_search}}" type="submit" />
        </form>

        <div id="scroll">
            {{ !sch_res }}
        </div>
    </main>

    % include('footer.tpl')
    % include('scripts.tpl')

    <script src="/static/sorttable.js"></script>
    <script src="/static/perfect-scrollbar.min.js"></script>

    <script>
        window.onload = function() {
            sorttable.innerSortFunction.apply(document.getElementById("seeds"), []);
            var container = document.getElementById("scroll");
            Ps.initialize(container);
        }
    </script>
</body>
</html>


