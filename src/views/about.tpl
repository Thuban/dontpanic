<!doctype html>
<html>
%include('head.tpl')
<body>
    <div class="middle-center">
        <h1 style="font-size: 60px;">
            {{title}}
            <img src="/static/{{logo}}" height=60 width=60>
        </h1>
        <br/>

        <h3 style="font-size: 40px;">{{version}}</h3>
        <br/><br/><br/><br/>

        <div style="font-size: 21px; line-height:1.15em;margin-top:1em">{{ !htmlstuff }}</div>
    </div>

    % include('footer.tpl')
    % include('scripts.tpl')
</body>
</html>
