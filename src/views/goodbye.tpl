<!doctype html>
<html>
%include('head.tpl')
<body>
    <div class="middle-center">
        <h1 style="font-size: 50px;">{{title}}</h1>
        <br/><br/>
        <div style="font-size: 25px; line-height:1.15em;margin-top:1em;">{{ !htmlstuff }}</div>
    </div>

    % include('footer-gb.tpl')
    % include('scripts.tpl')
</body>
</html>


