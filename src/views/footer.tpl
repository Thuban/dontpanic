<footer>
    <p class="float-left">
        <a href="/me">{{tpl_footer_your}}</a> -
        <a href="/_admin">{{tpl_admin}}</a> -
        <a href="/_about">{{tpl_about}}</a> -
        <a href="/_goodbye">{{tpl_stopdp}}</a>
    </p>
    <p class="float-right">
        <a href="https://unsplash.com/">{{tpl_footer_bg}}</a> -
        <a href="http://3hg.toile-libre.org/dontpanic">Don't Panic</a>
    </p>
</footer>
