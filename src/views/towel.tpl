<!doctype html>
<html>
%include('head.tpl')
<body>
    <header>
        <h1>
            <img src="/static/{{logo}}" height=25 width=25>
            {{welcomemsg}}
        </h1>
    </header>

    <main>
        <form class="searchbar" action="/search" method="get">
            <input name="keywords" type="text" placeholder="{{tpl_movie_or_serie}}" autofocus>
            {{!search_engines}}
            <input value="{{tpl_search}}" type="submit">
        </form>

        %if recents != '':
        <div>
            {{ !recents }}
        </div>
        %end

    </main>
    %if islastv == False:
    <a class="toupgradealert" href="http://3hg.toile-libre.org/dontpanic/download.html" target="_blank">{{tpl_not_last_version}}</a>
    %end

    % include('footer.tpl')
    % include('scripts.tpl')

    <script>
        $(function () {
          $('[data-toggle=tooltip]').tooltip();
        });
    </script>
</body>
</html>


