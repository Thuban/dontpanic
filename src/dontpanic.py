#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Auteur :      thuban <thuban@yeuxdelibad.net>
licence :     MIT

Description :
    Just watch your favorite show and keep track of them.
"""

import getpass
import sys
import os
from argparse import ArgumentParser

from dontpanic.ui import *
logger = logging.getLogger(__name__)



if __name__ == '__main__':
    parser = ArgumentParser(prog=progname, description=_("Dontpanic is a python app to look for any movie or"
                                                         " serie and watch them in streaming. It looks like popcorntime"
                                                         ", but doesn't depend of a centralized api."))

    parser.add_argument("-v", "--verbose", action="count", default=0, help=_("Increase output verbosity"))
    parser.add_argument("--version", action="version", version=progversion, help=_("Print version"))
    parser.add_argument("magnet", nargs='?', action="store", default="", help=_("Stream a magnet link"))

    args = parser.parse_args()
    setup_logging(args.verbose)

    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    prepconfig()

    port = lock()

    me = getpass.getuser()

    # a magnet link is passed as argument
    mgt = ""
    if args.magnet.startswith("magnet:"):
        mgt = quoteurl(args.magnet)

    if not port:   # already running
        with open(lock_file, 'r') as l:
            port = l.read().strip()
        if mgt != "":
            webbrowser.open("http://localhost:{}/stream/dontpanic/{}/0".format(port, mgt))
        else:
            webbrowser.open("http://localhost:{}".format(port))
    else:
        myapp = DontPanic('localhost', port, me, magnet=mgt)
        myapp.run()

    myapp = DontPanic('localhost', port, me, magnet=mgt)
    myapp.run()

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
