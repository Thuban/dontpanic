#!/bin/sh
#Auteur :      thuban <thuban@yeuxdelibad.net>

REP=~/geek/gitreps/3hg/dontpanic
OUT=~/geek/gitreps/3hg/3hg-website/website/dontpanic/downloads/

if [ -d /tmp/dontpanic ]; then
    rm -rf /tmp/dontpanic
fi

mkdir -p /tmp/dontpanic
cd $REP
cp -r * /tmp/dontpanic
rm -r /tmp/dontpanic/site
cd /tmp
tar cvzf "dontpanic-latest.tgz"  "dontpanic"
mv /tmp/dontpanic-latest.tgz $OUT/

exit

