# To generate the translation :
pygettext -o locale/dontpanic.pot dontpanic.py dontpanic-configuration.py hm_utils.py
cd locale

msginit -i dontpanic.pot -o fr/LC_MESSAGES/dontpanic.po
msginit -i dontpanic.pot -o en/LC_MESSAGES/dontpanic.po
...

# Edit the .po to translate, then in the folder of the translate .po, run :
msgfmt dontpanic.po -o dontpanic.mo

#-------------------------------------------------

# Update of the translation
# Creation of a new pot file
pygettext -o locale/dontpanic.pot dontpanic.py dontpanic-configuration.py hm_utils.py

# Update of files' translation
cd locale
msgmerge --update fr/LC_MESSAGES/dontpanic.po dontpanic.pot
