Don't panic
===========

Dontpanic is a python app to look for any movie or serie and watch them
in "streaming". It looks like popcorntime, but doesn't depend of a
centralized api.

![apercu de dontpanic](http://3hg.toile-libre.org/dontpanic/Images/dontpanic1.png)

Installation
------------

To use it, get the source archive

    wget https://framagit.org/Thuban/dontpanic/repository/master/archive.tar.gz

then install these dependencies : 

    python3-bs4 python3-libtorrent python3-requests python3-bottle sqlite3 gettext xdg-utils

Usage
-----

You can run dontpanic for a single user with : 

    ./dontpanic 

or with

    python3 dontpanic.py

It will open your browser to the dontpanic page.


My download doesn't start
-------------------------
Be sure to choose a file with a lot of peers.

Deploy on a server
------------------
You can run dontpanic on your server and make it available for world. To
do so, you have to use an adapter like [cherrypy](http://www.cherrypy.org/) or tornado.

    ./dontpanic -adapter administrator admin_password

or

    python3 dontpanic-server.py -adapter administrator admin_password

Change `-adapter` to the one you want to use, i.e. `-cherrypy`. Below,
you can find some adapters (see
[here](http://bottlepy.org/docs/dev/deployment.html#switching-the-server-backend)
for a full list):

-  `-cherrypy` is multi-threaded (recommended). Install python-cherrypy3 to use it.
- `-paste`  is multi-threaded. Instal python-paste to use it.
- `-tornado` : install python-tornado
- ...

You can use `-auto` to let dontpanic (bottle) choose the first available
adapter.

Use SSL (https):
----------------
To do it correctly, you might want to read your adapter documentation.

An easy way is to use apache or nginx as a reverse proxy.
For cherrypy, it's documented [here](http://docs.cherrypy.org/en/latest/deploy.html#reverse-proxying).

Below, find a working nginx configuration using ssl with cherrypy : 




    upstream apps {
        server 127.0.0.1:8080;
    }

    gzip_http_version 1.0;
    gzip_proxied      any;
    gzip_min_length   500;
    gzip_disable      "MSIE [1-6]\.";
    gzip_types        text/plain text/xml text/css
                    text/javascript
                    application/javascript;


    server {
        listen 80;
        server_name localhost;
        return 301 https://$server_name$request_uri;  # enforce https
    }

    server {
        listen 443 ssl;
        server_name  localhost;

        ssl_certificate /etc/ssl/certs/ssl-cert-snakeoil.pem;
        ssl_certificate_key /etc/ssl/private/ssl-cert-snakeoil.key;

    location / {
        proxy_pass         http://apps;
        proxy_redirect     off;
        proxy_set_header   Host $host;
        proxy_set_header   X-Real-IP $remote_addr;
        proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   X-Forwarded-Host $server_name;
    }
    }


Of course, replace `localhost`, `8080` and ssl variables to fit your
configuration.

In this case, dontpanic was started with : 

    python dontpanic-server.py -cherrypy admin password 8080

Warning
----------

If you deploy dontpanic on a server, make sure the database is not
readable by world. The database is by default in
~/.dontpanic/databaz.db. Follow 
[bottle instructions](http://bottlepy.org/docs/dev/deployment.html).

Remember, the admin page is /_admin

Creating debian package
-----------------------

To create the debian package, use equivs : 

    equivs-build eqctl

Add `-f` to sign the package.

Thanks
------
- Thanks to imdb, piratebay, btdigg, torrentproject and other search engines for results.
- Thanks to [thesubdb](http://thesubdb.com/) for providing subtitles to
  opensources projects
- Thanks to Douglas Adams for inspiration
